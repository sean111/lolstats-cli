<?php
require_once 'parallelcurl.php';
//Config
$dbHost = "localhost";
$dbUser = "lolstats";
$dbPassword = "aKcQ9vtbvhz2SGEp";
$dbName = "lolstatsv3";
$maxrequests = 10;

$header = array(
    "Content-Type: text/html; charset\"utf-8\"",
    "Accept-Charset: utf-8" ,
    "X-Developer-ID: F2BD5A84-C863-4C9E-A53C-A0894E501DCE",
    "X-Application-ID: 033FFE45-0D23-4C07-A915-963223975C16"
);

$curl_options = array(
    CURLOPT_HTTPHEADER => $header,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_ENCODING => 'gzip',
    CURLOPT_FRESH_CONNECT => true,
);

//c0de
$db = new PDO("mysql:host=$dbhost;dbname=$dbName", $dbUser, $dbPassword);
$pc = new ParallelCurl($maxrequests, $curl_options);

var_dump($argv);

switch($argv[1]) {
    default: {
        update_summoners();
    }
    break;
    case "summoner": {
        update_summoners($argv[2]);
    }
    break;
    case "champions": {
        update_champions();
    }
    break;
    case "spells": {
        update_spells();
    }
    break;
    case "items": {
        update_items();
    }
    break;
}

function update_summoners($summoner = null) {
    global $pc, $db;
    if(!empty($summoner)) {
        $sql = "SELECT s.id, s.name, r.code as region FROM summoners s INNER JOIN regions r ON s.region=r.id WHERE s.name = '$summoner'";
    }
    else {
        $sql = "SELECT s.id, s.name, r.code as region FROM summoners s INNER JOIN regions r ON s.region=r.id ORDER BY name";
    }
    print "Query: $sql\n";
    foreach($db->query($sql) as $summoner) {
        print $summoner['name']." [$summoner[region]]\n";
        $url = "http://api.captainteemo.com/player/$summoner[region]/".urlencode($summoner['name'])."/recent_games";
        print "URL: $url\n";
        $pc->startRequest($url, 'parse_games', array('id' => $summoner['id'], 'name'=> $summoner['name']));
        $url2 = "http://api.captainteemo.com/player/$summoner[region]/".urlencode($summoner['name']);
        print "URL2: $url2\n";
        $pc->startRequest($url2, 'parse_summoner', array('id' => $summoner['id']));
    }
    $pc -> finishAllRequests();
}

function parse_summoner($content, $url, $ch, $vars) {
    global $db;
    $data = json_decode($content, true);
    $data = $data['data'];
    $sql = "UPDATE summoners SET level = $data[level], icon = $data[icon] WHERE id = $vars[id]";
    //print "SQL: $sql";
    $db->exec($sql);
}

function parse_games($content, $url, $ch, $vars) {
    global $db;
     $data = json_decode($content, true);
    $games = $data['data']['gameStatistics']['array'];
    for($x =0; $x < sizeof($games); $x++) {
        $game = $games[$x];
        $date = strtotime($game[createDate]);

        //$sql = "SELECT id FROM macthes WHERE summoner = $vars[id] AND `date` = $date";
        $sql = $db->prepare("SELECT id FROM matches WHERE summoner = ? AND `date` = ?");
        $sql->execute(array(
            $vars['id'],
            $date
        ));
        $res = $sql->fetch();
        if(empty($res['id'])) {
            /*print "Date: $game[createDate] [$date]\n";
            print "\tChampion: $game[championId]\n";
            print "\tSkin: $game[skinIndex]\n";
            print "\tGame Type: $game[gameType]\n";
            print "\tGame Mode: $game[gameMode]\n";
            print "\tSpell1: $game[spell1]\n";
            print "\tSpell2: $game[spell2]\n";*/

            //$sql = "INSERT INTO matches (summoner, date, gameMode, gameType, champion, skin, spell1, spell2) VALUES ($vars[id],  $date, '$game[gameMode]', '$game[gameType]', $game[championId], $game[skinIndex], $game[spell1], $game[spell2])";
            $sql =$db->prepare("INSERT INTO matches (summoner, date, gameMode, gameType, queueType, subType, champion, skin, spell1, spell2) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $sql->execute(array(
                $vars['id'],
                $date,
                $game['gameMode'],
                $game['gameType'],
                $game['queueType'],
                $game['subType'],
                $game['championId'],
                $game['skinIndex'],
                $game['spell1'],
                $game['spell2']
            ));
            $matchId = $db->lastInsertId();
            //print "Match ID: $matchId\n";
            $stats = $game['statistics']['array'];
            $statSql = $db->prepare("INSERT INTO match_stats (`match`, `name`, `value`) VALUES (?, ?, ?)");
            for($y = 0; $y < sizeof($stats); $y++) {
                $stat = $stats[$y];
                if($stat['statType']=="WIN") {
                    $won = 1;
                }
                else if($stat['statType']=='LOSE') {
                    $won = 0;
                }
                $statSql->execute(array(
                    $matchId,
                    $stat['statType'],
                    $stat['value']
                ));
            }

            $sql = "UPDATE matches SET win = $won WHERE id = $matchId";
            $db->exec($sql);

            //print "\tResult: ".(($won) ? 'Win' : 'Loss')."\n";
            //break;
        }
    }
}

function update_champions() {
    global $curl_options, $db;
    print "Updating champions\n";
    $cp = curl_init();
    curl_setopt_array($cp, $curl_options);
    $url = "http://api.captainteemo.com/datadragon/champion";
    //print "URL: $url\n";
    curl_setopt($cp, CURLOPT_URL, $url);
    $db->exec("TRUNCATE TABLE champions");
    $data = curl_exec($cp);
    $data = json_decode($data, true);
    $champions = $data['data'];
    $champions = array_values($champions);
    for($x = 0; $x <sizeof($champions); $x++) {
        $champ = $champions[$x];
        /*print "Champion: $champ[name]\n";
        print "\tID: $champ[key]\n";
        print "\tTitle: $champ[title]\n";
        print "\tTags:\n";
        print "\t\t1: ".$champ['tags'][0]."\n";
        print "\t\t2: ".$champ['tags'][1]."\n";
        print "\t\t3: ".$champ['tags'][2]."\n";
        print "\tResource: $champ[partype]\n";*/
        $tag1 = $champ['tags'][0];
        $tag2 = $champ['tags'][1];
        $tag3 = $champ['tags'][2];
        //$sql = "INSERT INTO champions (id, name, title, tag1, tag2, tag3, resource) VALUES ($champ[key], '$champ[name]',  '$champ[title]', '$tag1', '$tag2', '$tag3', '$champ[partype]')";
        $sql = $db->prepare("INSERT INTO champions (id, name, title, tag1, tag2, tag3, resource) VALUES (?, ?, ?, ?, ?, ?, ?)");
        //print "SQL: $sql\n";
        $sql->execute(array(
            $champ['key'],
            $champ['name'],
            $champ['title'],
            $tag1,
            $tag2,
            $tag3,
            $champ['partype']
        ));
    }
    print "Champion list updated....\n";
    curl_close($cp);
}

function update_spells() {
    global $curl_options, $db;
    print "Updating spells\n";
    $db->exec("TRUNCATE TABLE spells");
    $cp = curl_init();
    curl_setopt_array($cp, $curl_options);
    $url = "http://api.captainteemo.com/datadragon/summoner";
    //print "URL: $url\n";
    curl_setopt($cp, CURLOPT_URL, $url);
    $data = curl_exec($cp);
    $data = json_decode($data, true);
    $spells = array_values($data['data']);
    for($x = 0; $x < sizeof($spells); $x++) {
        $spell = $spells[$x];
        print "$spell[name] [$spell[key]]\n";
        //$sql = "INSERT INTO spells (id, name) values ($spell[key], '$spell[name]')";
        //print "SQL: $sql\n";
        $db->exec($sql);
    }
    curl_close($cp);
    print "Spell list updated...\n";
}

function update_items() {
    global $curl_options, $db;
    print "Updating items\n";
    $db->exec("TRUNCATE TABLE items");
    $cp = curl_init();
    curl_setopt_array($cp, $curl_options);
    $url = "http://api.captainteemo.com//datadragon/item";
    print "URL: $url\n";
    curl_setopt($cp, CURLOPT_URL, $url);
    $data = curl_exec($cp);
    $data = json_decode($data, true);
    $items = $data['data'];
    foreach($items as $key => $item) {
        //print "Key: $key\n";
        //print $item['name']." [$key]\n";
        $sql = "INSERT INTO items (id, name) VALUES ($key, '$item[name]')";
        $db->exec($sql);
    }
    print "Item list updated...";
    curl_close($cp);
}

function old() {
    $sql = "SELECT s.id, s.name, r.code as region FROM summoners s INNER JOIN regions r ON s.region=r.id ORDER BY name";
    foreach($db->query($sql) as $summoner) {
        print $summoner['name']." [$summoner[region]]\n";
        $url = "http://api.captainteemo.com/player/$summoner[region]/".urlencode($summoner['name'])."/recent_games";
        print "URL: $url\n";
        $pc->startRequest($url, 'parse_games', array('id' => $summoner['id'], 'name'=> $summoner['name']));
        break;
    }
    $pc -> finishAllRequests();
    function parse_games($content, $url, $ch, $vars) {
        var_dump($vars);
        $data = json_decode($content, true);
        var_dump($data);
    }
}
?>
