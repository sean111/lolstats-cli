<?php
include 'elophant.php';
$e = new elophantStats('ki6uyLMr3orPeoQKfhi2');

/*print "Server Statuses:\n";
print "-------------\n";
$serverStatus = $e->getStatus();
foreach($serverStatus as $server => $status) {
    $status = $status ? 'UP' : 'DOWN';
    print "\t$server: $status\n";
}*/

//Champion List Test
print "Champion List\n";
print "-------------\n";
$champs = $e->getChampions();
for($x=0; $x<sizeof($champs); $x++) {
	$champ= $champs[$x];
	print "\t[$champ[id]] $champ[name]\n";
}

//Item List Test
print "Item List\n";
print "---------\n";
$items = $e->getItems();
for($x=0; $x<sizeof($items); $x++) {
	$item = $items[$x];
	print "\t[$item[id]] $item[name] { $item[iconPath] }\n";
}


//Summoner Data Test
$info = $e->getSummonerInfo('Daboz', "NA");
var_dump($info);
print "Player: Daboz\n";
print "-------------\n";
print "\tAccount ID: $info[acctId]\n";
print "\tSummoner ID: $info[summonerId]\n";
print "\tSummoner Level: $info[summonerLevel]\n";


//Recent Games Test
print "Recent Games Test\n";
print "--------------------------\n";
$games = $e->getRecentGames($info['acctId'], 'na');
$game = $games[0];
var_dump($game);
print "Date: $game[createDate]\n";
print "Game: $game[gameMode]\n";
print "SubMode: $game[subType]\n";
print "Date [RAW]: $game[createDate]\n";
$date = str_replace(array("/","Date(",")"), "", $game['createDate']);
$date = intval($date);
print "Date [String Removed]: $date\n";
//$tdate = $e->winToUnixTime($date);
//print "Date [unixts]: $tdate\n";
$tdate =  ($date / 1000) + (23 * 3600); //Changes miliseconds to seconds and fixes a time difference issue (23hrs off)
print "MS to S = $tdate\n";
print "Date [String]: ".date('m/d/Y h:ia', $tdate)."\n";

//Team Data Test
$team = $e->findTeam("ohmahgerdz", "na");
//var_dump($team['teamStatSummary']['teamStatDetails']);
$stats = $team['teamStatDetails'];

for($x=0; $x<sizeof($stats); $x++) {
    $stat = $stats[$x];
    print "$stat[teamStatType] - $stat[wins]/$stat[losses]\n";
}
?>