<?php
/**
* API wrapper for the elophant API
* @package elophant
* @version 0.1
* @author Sean Burke (sean111@gmail.com)
*/

/**
* elophantStats class, this gets the data we want
* @access public
*/
class elophantStats {
    var $apiKey, $cp, $header;

    /**
    * Construct for the class
    * @param $apiKey API Key
    * @access public
    */
    public function __construct($apiKey = null, $parallel = false) {
        if($apiKey) {
            $this->apiKey = $apiKey;
        }
        if($parallel) {
            $this->pcurl = true;
            require_once 'parallelcurl.php';
        }
        $header=array(
            "Content-Type: text/html; charset\"utf-8\"",
            "Accept-Charset: utf-8"
        );

        $curl_options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_FRESH_CONNECT => true
        );

        if($parallel) {
            $this->cp = new ParallelCurl(10, $curl_options);
        }
        else {
             $cp = curl_init();
            curl_setopt_array($cp, $curl_options);
            $this->cp = $cp;
        }
    }

    public function getChampions() {
        $url = "http://api.elophant.com/v2/champions?key=$this->apiKey";
        if($this->canConnect()) {
            curl_setopt($this->cp, CURLOPT_URL, $url);
            $data = curl_exec($this->cp);
            $data = json_decode($data, true);
            if($data['success']!=true) {
                return false;
            }
            $data = $data['data'];
            return $data;
        }
        else {
            return false;
        }
    }

    public function getItems() {
        $url = "http://api.elophant.com/v2/items?key=$this->apiKey";
        print $url."\n";
        if($this->canConnect()) {
            curl_setopt($this->cp, CURLOPT_URL, $url);
            $data = curl_exec($this->cp);
            $data = json_decode($data, true);
            if($data['success']!=true) {
                return false;
            }
            $data = $data['data'];
            return $data;
        }
        else {
            return false;
        }
    }

    public function getSummonerInfo($name, $server) {
        $server = strtolower($server);
        $name = str_replace(" ", "%20", $name);
        $url = "http://api.elophant.com/v2/$server/summoner/$name?key=$this->apiKey";
        print $url."\n";
        if($this->canConnect()) {
            curl_setopt($this->cp, CURLOPT_URL, $url);
            $data = curl_exec($this->cp);
            $data = json_decode($data, true);
            if($data['success']!=true) {
                return false;
            }
            $data = $data['data'];
            return $data;
        }
        else {
            return false;
        }
    }

    public function getRecentGames($accountId, $server) {
        $server = strtolower($server);
        $url = "http://api.elophant.com/v2/$server/recent_games/$accountId?key=$this->apiKey";
        print "\tURL: $url\n";
        if($this->canConnect()) {
            curl_setopt($this->cp, CURLOPT_URL, $url);
            $data = curl_exec($this->cp);
            $data = json_decode($data, true);
            if($data['success']!=true) {
                print $data['error']."\n";
                return false;
            }
            return $data['data']['gameStatistics'];
        }
        else {
            return false;
        }
    }

    public function findTeam($name, $server) {
        $name = strtolower($name);
        $name = str_replace(' ', '%20',  $name);
        $server = strtolower($server);
        $url = "http://api.elophant.com/v2/$server/find_team/$name?key=$this->apiKey";
        print $url."\n";
        if($this->canConnect()) {
            curl_setopt($this->cp, CURLOPT_URL, $url);
            $data = curl_exec($this->cp);
            $data = json_decode($data, true);
            if($data['success']!=true) {
                print $data['error']."\n";
                return false;
            }
            return $data['data']['teamStatSummary'];
        }
        else {
            return false;
        }
    }


    //*********************
    //*  Private Functions   *
    //*********************
    private function canConnect() {
        if(empty($this->apiKey)) {
            throw new Exception("No API key defined", 1);
        }

        if(empty($this->cp)) {
            throw new Exception("Curl not initialized", 2);
        }
        return true;
    }

    private function throwError($error) {
        if(empty($error)) {
            $error = "No error message given";
        }
        throw new Exception($error);
    }
}
?>