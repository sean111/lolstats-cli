<?php
$header = array(
    "Content-Type: text/html; charset\"utf-8\"",
    "Accept-Charset: utf-8" ,
    "X-Developer-ID: F2BD5A84-C863-4C9E-A53C-A0894E501DCE",
    "X-Application-ID: 033FFE45-0D23-4C07-A915-963223975C16"
);

$curl_options = array(
    CURLOPT_HTTPHEADER => $header,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_ENCODING => 'gzip',
    CURLOPT_FRESH_CONNECT => true
);

$summoner = 'Daboz';
$region = "na";

$cp = curl_init();
curl_setopt_array($cp, $curl_options);

$url = "http://api.captainteemo.com/player/$region/".urlencode($summoner)."/recent_games";
//$url = "http://api.captainteemo.com/datadragon/champion";
print "URL: $url\n";
curl_setopt($cp, CURLOPT_URL, $url);
$data = curl_exec($cp);

parse_games($data, $url, null, array('id'=>1, 'name'=>'Daboz'));

function parse_games($content, $url, $ch, $vars) {
    $data = json_decode($content, true);
    $games = $data['data']['gameStatistics']['array'];
    for($x =0; $x < sizeof($games); $x++) {
        $game = $games[$x];
        print "Date: $game[createDate]\n";
        print "\tChampion: $game[championId]\n";
        print "\tSkin: $game[skinIndex]\n";
        print "\tGame Type: $game[gameType]\n";
        print "\tGame Mode: $game[gameMode]\n";
        print "\tSpell1: $game[spell1]\n";
        print "\tSpell2: $game[spell2]\n";
        $stats = $game['statistics']['array'];
        for($y = 0; $y < sizeof($stats); $y++) {
            $stat = $stats[$y];
            if($stat['statType']=="WIN") {
                $won = true;
            }
            else if($stat['statType']=='LOSE') {
                $won = false;
            }
        }
        print "\tResult: ".(($won) ? 'Win' : 'Loss')."\n";
        //break;
    }
}
?>